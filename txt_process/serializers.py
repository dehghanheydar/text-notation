from abc import ABC

from rest_framework import serializers
from .models import Document, Sentence, Genre, Paragraph
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = "__all__"

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class DocumentSerializer(serializers.Serializer):
    type_CHOICES = (
        ('F', 'فارسی'),
        ('I', 'آوانگاری شده'),
    )
    text = serializers.CharField(label='نام', required=True, error_messages={'required': 'یک نام برای پیکره خود انتخاب کنید.'})
    my_doc = serializers.FileField(label='انتخاب فایل')
    type = serializers.ChoiceField(choices=type_CHOICES, label='انتخاب نوع')


class DocumentModelSerializer(serializers.ModelSerializer):
    token_num = serializers.SerializerMethodField('is_token_num')
    sentence_num = serializers.SerializerMethodField('is_sentence_num')

    class Meta:
        model = Document
        fields = ['pk', 'user', 'name', 'text', 'type', 'token_num', 'sentence_num']

    def is_token_num(self, document):
        return document.token_set.all().count()

    def is_sentence_num(self, document):
        return document.sentence_set.all().count()


class SentenceModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sentence
        fields = '__all__'


class SentenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sentence
        fields = ['text', ]


class ParagraphModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Paragraph
        fields = '__all__'


class GenreModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Genre
        fields = '__all__'
