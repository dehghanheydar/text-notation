from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django.core.exceptions import ValidationError


# class RegisterForm(UserCreationForm):
#     email = forms.EmailField()
#
#     class Meta:
#         model = User
#         fields = ["username", "email", "password1", "password2"]


class RegisterForm(forms.Form):
    username = forms.CharField(label='نام کاربری', min_length=4, max_length=150,
                               error_messages={'min_length': 'اطمینان حاصل کنید که طول نام کاربری حداقل از 4  بیشتر باشد '})
    email = forms.EmailField(label='examp@examp.com', error_messages={'invalid': 'یک آدرس ایمیل معتبر وارد کنید'})
    password1 = forms.CharField(label='رمز', widget=forms.PasswordInput)
    password2 = forms.CharField(label=' تأیید رمز', widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("نام کاربری از قبل وجود دارد.")
        return username

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        print(r)
        if r.count():
            raise ValidationError("این ایمیل از قبل وجود دارد.")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("رمز عبور مطابقت ندارد.")

        return password2

    def save(self, commit=True):
        user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password1']
        )
        return user