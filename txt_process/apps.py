from django.apps import AppConfig


class TxtProcessConfig(AppConfig):
    name = 'txt_process'
