
from __future__ import unicode_literals
import re
from nltk.tokenize.api import TokenizerI


class SentenceTokenizer(TokenizerI):

    def __init__(self):
        self.pattern = re.compile(r'([!\.\?⸮؟]+)[ \n]+')

    def tokenize(self, text):
        text = self.pattern.sub(r'\1\n\n', text)
        return [sentence.replace('\n', ' ').strip() for sentence in text.split('\n\n') if sentence.strip()]



# text = 'خواهش ما از شما این است کهِ پیشاًپِیُشَ نگرٌاٌن اتفاقاتی که امسال قرار است رخ بدهد، نباشید! حتی لازم نیست نگران اتفاقاتی که این ماه قرار است رخ بدهد، باشید! تنها کاری که شما باید انجام دهید، این است که فقط به امروزتان فکر کنید. آیا واقعاً می‌توانید امروز را درست سپری کنید؟'
#
# x= SentenceTokenizer().tokenize(text.encode('utf-8'))
#
# print(x)