from django.urls import path, include, re_path
from txt_process import views as txt_process_view

urlpatterns = [
    path("register/", txt_process_view.signup, name="register"),
    path('', txt_process_view.GetInfo.as_view(), name='home'),
    path('ducument/<int:pk>/', txt_process_view.DocumentDetail.as_view(), name='get_doc'),
    path('delet-ducument/<int:pk>/', txt_process_view.DocumentDelete.as_view(), name='delete_doc'),

    path('graph', txt_process_view.show_genres, name='graph'),
    # path('ee', txt_process_view.show_genres2, name='i'),
    path('accounts/', include('django.contrib.auth.urls')),

    path('get-tree/<name>/', txt_process_view.TreeDetail.as_view(), name='get-tree'),
    # node do urls
    path('createNode/', txt_process_view.GenresDetail.as_view(), name='create_node'),

    path('setting/', txt_process_view.TreeListView.as_view(), name='setting'),

    #path('update-sentence/', txt_process_view.ParagraphDetail.as_view(), name='update-sentence'),
    re_path('edit-Sentence/(?P<pk>[0-9]+)/(?P<name>.*)/', txt_process_view.ParagraphDetail.as_view(), name='edit-sentence'),

    # path('edit-syntactic/', txt_process_view.ParagraphDetail.as_view(), name='edit-syntactic'),

    path('set-data-edit-syntactic/<int:pk>/<int:token_pk>/<slug:type>', txt_process_view.ParagraphSethDetail.as_view(), name='edit-syntactic-set-data'),

    path('set-data-edit-syntactic/', txt_process_view.ParagraphSethDetail.as_view(), name='edit-syntactic-set-data'),

    # get data for draw arc tree in sentence
    path('sentence-syntactic-tree-data/<int:pk>', txt_process_view.get_para_syntactic_tree_data,
         name='sentence-syntactic-tree-data'),

    path('delete-token-source-label/', txt_process_view.delete_token_source_label, name='delete-token-source-label'),

    path('morpho-analis/', txt_process_view.delete_token_source_label, name='delete-token-source-label'),

    path('token-edit/', txt_process_view.TokenDetail.as_view(), name='future-value-detail'),

    path('future-edit/', txt_process_view.FutureValueDetail.as_view(), name='future-value-detail'),

    path('future-edit/<int:pk>', txt_process_view.FutureValueDetail.as_view(), name='future-value-detail'),

    path('syntactic-edit/<int:pk>', txt_process_view.SyntacticDetail.as_view(), name='syntactic-detail'),

    path('create-file/<int:pk>', txt_process_view.download, name='create-file'),

    path('save-root/<int:pk>', txt_process_view.save_token_root, name='save_token_root'),

    path('new-sentence/<int:pk>', txt_process_view.edit_sentence, name='save_token_root'),

    path('get-sentence/', txt_process_view.get_sentence, name='get-sentence'),
]

