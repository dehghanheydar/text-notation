import mimetypes
import os
import threading
from time import sleep
from wsgiref.util import FileWrapper

from django.conf import settings
from django.utils.encoding import smart_str
from next_prev import next_in_order, prev_in_order

from .forms import RegisterForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from mptt.templatetags.mptt_tags import cache_tree_children

from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import (
    DocumentSerializer,
    DocumentModelSerializer,
    GenreModelSerializer)

from .my_hazm.Normalizer import Normalizer
from .my_hazm.SentenceTokenizer import SentenceTokenizer
from .my_hazm.WordTokenizer import WordTokenizer
from .my_hazm.Stemmer import Stemmer

from .models import (
    Document,
    Sentence,
    Paragraph,
    Token,
    Genre,
    FutureValue,
    DocumentFile
)

from .process import (
    recursive_node_to_dict,
    create_file,
    make_doc_file,

)
from rest_framework.decorators import api_view

from django.shortcuts import get_object_or_404

from django.views.generic.edit import DeleteView


class SyntacticDetail(LoginRequiredMixin, APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'txt_process/edit-syntactic.html'

    def get_object(self, pk):
        try:
            return Sentence.objects.get(pk=pk)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        sentence = self.get_object(pk=pk)
        obj = get_object_or_404(Genre, name='????', parent=None)
        labels = obj.get_children()
        # leaf = obj.get_leafnodes()

        try:
            priv = Sentence.objects.filter(pk__lt=pk, document_id=sentence.document.pk).order_by('pk').last()
            # priv = prev_in_order(sentence, loop=True)
        except Exception as e:
            priv = None

        try:
            next = Sentence.objects.filter(pk__gt=pk, document_id=sentence.document.pk).order_by('pk').first()
            # next = next_in_order(sentence, loop=True)
        except Exception as e:
            next = None

        try:
            num = list(Sentence.objects.filter(document_id=sentence.document.pk).order_by('pk')).index(sentence) + 1
        except Exception as en:
            print(en)

        return Response({'sentence': sentence, 'type': 'syntactic', 'labels': labels, 'next': next, 'priv': priv, 'num': num})


class ParagraphDetail(LoginRequiredMixin, APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'txt_process/edit-sentence.html'

    def get_object(self, pk):
        try:
            return Sentence.objects.get(pk=pk)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, pk, name):

        global num
        sentence = self.get_object(pk=pk)
        obj = get_object_or_404(Genre, name=name, parent=None)
        leve_one = obj.get_children()
        leaf = obj.get_leafnodes()

        try:
            priv = Sentence.objects.filter(pk__lt=pk, document_id=sentence.document.pk).order_by('pk').last()
            # priv = prev_in_order(sentence)
        except Exception as e:
            priv = None

        try:
            next = Sentence.objects.filter(pk__gt=pk, document_id=sentence.document.pk).order_by('pk').first()
            # next = next_in_order(sentence)
        except Exception as e:
            next = None


        try:
            num = list(Sentence.objects.filter(document_id=sentence.document.pk).order_by('pk')).index(sentence) + 1
        except Exception as en:
            print(en)


        try:

            if name == 'صرفی':
                return Response(
                    {'leaf': leaf, 'sentence': sentence, 'level_one': leve_one, 'type': 'morpho', 'next': next,
                     'priv': priv, 'num': num})

            elif name == 'نحوی':
                return Response(
                    {'leaf': leaf, 'sentence': sentence, 'level_one': leve_one, 'type': 'syntactic', 'next': next,
                     'priv': priv})

        except Exception as e:
            print(e)

    def put(self, request, pk, name):

        sentence = get_object_or_404(Sentence, pk=pk)

        textNormal = Normalizer().normalize(text=request.data['text'])

        sentence.text = textNormal
        sentence.save()

        for token in sentence.token_set.all():
            token.delete()

        for tok in WordTokenizer().tokenize(textNormal):
            root_word = Stemmer().stem(tok)
            tok_obj = Token(user=request.user, document=sentence.document, system_root=root_word, paragraph=sentence.paragraph,
                            sentence=sentence, text=tok)
            tok_obj.save()

        return Response(status=status.HTTP_202_ACCEPTED, data={'ok': 'ok'})


class FutureValueDetail(LoginRequiredMixin, APIView):
    def get_object(self, pk):
        try:
            return FutureValue.objects.get(pk=pk)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        obj = get_object_or_404(Genre, pk=pk)

        # childs = obj.get_children()
        # token = get_object_or_404(Token, pk=pk)

        childs = []
        for child in obj.get_children():
            childs.append({'name': child.name,
                           'pk': child.pk,
                           'future': child.futureFlag,
                           'tree_id': child.tree_id})

        return Response({'childs': childs})

    def post(self, request):

        obj = get_object_or_404(Genre, pk=request.data['future_pk'])
        # childs = obj.get_children()
        token = get_object_or_404(Token, pk=request.data['token_pk'])

        future = FutureValue(token_id=token.pk, future_id=obj.id)
        future.save()

        childs = []
        for child in obj.get_children():
            childs.append({'name': child.name,
                           'pk': child.pk,
                           'future': child.futureFlag,
                           'tree_id': child.tree_id,
                           'future_pk': future.pk})

        return Response({'childs': childs})

    def put(self, request):

        if request.data['type'] == 'FV':
            # future_obj = get_object_or_404(FutureValue,pk=request.data['future_pk'])
            obj = get_object_or_404(Genre, pk=request.data['future_value'])
            future_obj = FutureValue.objects.update_or_create(pk=request.data['future_pk'],
                                                              defaults={'futureValue_id': obj.pk})
        elif request.data['type'] == 'F':
            # future_obj = get_object_or_404(FutureValue,pk=request.data['future_pk'])
            obj = get_object_or_404(Genre, pk=request.data['future_node_pk'])
            token = get_object_or_404(Token, pk=request.data['token_pk'])
            future_obj = FutureValue.objects.update_or_create(pk=request.data['future_pk'],
                                                              defaults={'token_id': token.pk, 'future_id': obj.pk})

        childs = []
        for child in obj.get_children():
            childs.append({'name': child.name,
                           'pk': child.pk,
                           'future': child.futureFlag,
                           'tree_id': child.tree_id,
                           'future_pk': future_obj[0].pk})

        return Response({'childs': childs, 'future_pk': future_obj[0].pk})

    def delete(self, request):

        # print(1)

        if request.data['type'] == 'all':
            # print(2)
            token = FutureValue.objects.filter(token_id=request.data['token_pk'])
            for item in token:
                item.delete()
        elif request.data['type'] == 'one':
            obj = self.get_object(request.data['pk'])
            obj.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class TokenDetail(LoginRequiredMixin, APIView):

    def get_object(self, pk):
        try:
            return Token.objects.get(pk=pk)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        pass

    def post(self, request):
        pass

    def put(self, request):
        try:
            if request.data['type'] == 'source&target':
                sentence_pk = None
                for path in request.data['data']:
                    try:
                        token = Token.objects.get(pk=path['target']['id'])
                        token.source = None
                        token.save()
                        sentence_pk = token.sentence.pk

                    except Exception as e:
                        print(e)

                return Response(status=status.HTTP_202_ACCEPTED, data={'sentence_pk': sentence_pk})
            elif request.data['type'] == 'smallPOS':
                token = self.get_object(request.data['token_pk'])
                token.smallPOS = None
                token.save()
                return Response(status=status.HTTP_202_ACCEPTED, data={'ok': 'ok'})
            elif request.data['type'] == 'bigPOS':
                token = self.get_object(request.data['token_pk'])
                token.bigPOS = None
                token.smallPOS = None
                token.save()
                return Response(status=status.HTTP_202_ACCEPTED, data={'ok': 'ok'})

        except Exception as e:
            print(e)
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        pass


class ParagraphSethDetail(APIView):

    def get_object(self, pk):
        try:
            return Genre.objects.get(pk=pk)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, pk, token_pk, type):

        obj = get_object_or_404(Genre, pk=pk)
        # childs = obj.get_children()
        token = get_object_or_404(Token, pk=token_pk)

        if type == 'S':
            token.smallPOS = obj
            token.save()
        elif type == 'B':
            token.bigPOS = obj
            token.save()

        childs = []
        for child in obj.get_children():
            childs.append({'name': child.name,
                           'pk': child.pk,
                           'future': child.futureFlag,
                           'tree_id': child.tree_id, })

        return Response({'childs': childs})

    def post(self, request):
        pass

    def put(self, request):

        if request.data['type'] == 'morpho':
            token = get_object_or_404(Token, pk=request.data['pk'])
            token.smallPOS = None
            token.bigPOS = None
            token.save()
            for item in FutureValue.objects.filter(token_id=token.pk):
                item.delete()

        elif request.data['type'] == "syntactic":

            source = get_object_or_404(Token, pk=request.data['source_id'])

            target = get_object_or_404(Token, pk=request.data['target_id'])

            target.source = source
            target.target_label = Genre.objects.get(pk=request.data['label_pk'])
            target.save()

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response({'ok': 'ok'})


class TreeListView(LoginRequiredMixin, APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "txt_process/setting.html"

    def get(self, request):
        roots = GenreModelSerializer(Genre.objects.filter(level=0), many=True)

        return Response({'root_list': roots.data})


class TreeDetail(LoginRequiredMixin, APIView):

    def get(self, request, name):
        obj = get_object_or_404(Genre, name=name, parent=None)

        return Response(recursive_node_to_dict(obj))


def save_document(obj, user, list_text_normal):
    try:
        if obj.type == 'I':
            for parg in list_text_normal:

                parag_obj = Paragraph(user=user, document=obj, text=str(parg), type=obj.type)
                parag_obj.save()

                for sentence in parg.split('.'):
                    sent_obj = Sentence(user=user, document=obj, paragraph=parag_obj, text=str(sentence),
                                        type=obj.type)
                    sent_obj.save()

                    for tok in sentence.split():
                        tok_obj = Token(user=user, document=obj, paragraph=parag_obj, sentence=sent_obj,
                                        text=str(tok), type=obj.type)
                        tok_obj.save()

        elif obj.type == 'F':
            for parg in list_text_normal:

                parag_obj = Paragraph(user=user, document=obj, text=str(parg), type=obj.type)
                parag_obj.save()
                for sentence in SentenceTokenizer().tokenize(parg):
                    sent_obj = Sentence(user=user, document=obj, paragraph=parag_obj, text=str(sentence),
                                        type=obj.type)
                    sent_obj.save()
                    for tok in WordTokenizer().tokenize(sentence):
                        root_word = Stemmer().stem(tok)
                        tok_obj = Token(user=user, document=obj, system_root=root_word, paragraph=parag_obj,
                                        sentence=sent_obj,
                                        text=str(tok), type=obj.type)
                        tok_obj.save()

    except Exception as e:
        return Response({'error': '?? ???? ??? ????? ????? ???? ???? .?? ??? ???? ?? ?? ?????? ?????? ???? ???.'})


class GetInfo(LoginRequiredMixin, APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "txt_process/get_text_doc.html"
    login_url = '/accounts/login'
    redirect_field_name = 'home'

    def get(self, request):
        serializer = DocumentSerializer()
        documents = DocumentModelSerializer(Document.objects.filter(user=request.user), many=True)
        return Response({'serializer': serializer, 'doc_serializer': documents.data})

    def post(self, request):

        serializer = DocumentSerializer(data=request.data)
        if serializer.is_valid():
            text = request.data['my_doc'].read().decode("utf-8")
            textNormal = Normalizer().normalize(text=text)
            list_text_normal = textNormal.split('\n\n')
            model_serializer = DocumentModelSerializer(data={'name': request.data['text'], 'text': str(text),
                                                             'type': request.data['type']})
            if model_serializer.is_valid():
                model_serializer.save()
                try:
                    obj = Document.objects.get(pk=model_serializer.data['pk'])
                    obj.user = request.user
                    obj.save()

                    t = threading.Thread(target=save_document, args=[obj, request.user, list_text_normal], daemon=True)
                    t.start()

                    # create_file(obj.pk)

                    sleep(5)
                    # make_doce_file(obj.pk)

                except Exception as e:
                    print(e)
                    response = redirect('/')
                    return response
                    # return Response(
                    #     {'error': '?? ???? ??? ????? ????? ???? ???? .?? ??? ???? ?? ?? ?????? ?????? ???? ???.'})

            else:
                print(model_serializer.error_messages)
                return Response({'error': model_serializer.error_messages})

            response = redirect(f'/ducument/{obj.pk}/')
            return response

        else:
            response = redirect('/')
            return response


class DocumentDetail(LoginRequiredMixin, APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "txt_process/get_text_doc.html"

    def get_object(self, pk):
        try:
            return Document.objects.get(pk=pk)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):

        snippet = self.get_object(pk)
        doc_serializer = DocumentModelSerializer(Document.objects.filter(user=request.user), many=True)
        serializer = DocumentSerializer()
        return Response({'serializer': serializer, 'text2': snippet, 'doc_serializer': doc_serializer.data})

    def delete(self, request, pk):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DocumentDelete(DeleteView):
    model = Document
    success_url = "/"


class GenresDetail(LoginRequiredMixin, APIView):

    def get(self, request):
        pass

    def post(self, request):

        # print(request.data)

        if request.data['future'] == "false":

            future = False
        else:
            future = True

        if request.data['parent_id'] == 'root':
            if not Genre.objects.filter(name=request.data['node_name'], parent=None, level=0).exists():
                g = Genre(name=request.data['node_name'], futureFlag=future, color=None)
                g.save()

                data = {'name': g.name,
                        'id': g.id,
                        'depth': g.get_level(),
                        'tree_id': g.tree_id,
                        'future': g.futureFlag
                        }

                return Response(data=data, status=status.HTTP_201_CREATED)

        else:

            # if not Genre.objects.filter(name=request.data['node_name'],
            #                             tree_id=request.data['parent_name_tree_id']).exists():
            print(request.data['color'])
            if request.data['color'] == '#FFFFFF':
                color = None
            else:
                color = request.data['color']

            if not Genre.objects.filter(name=request.data['node_name'],
                                        parent_id=request.data['parent_id']).exists():
                g = Genre(name=request.data['node_name'], parent_id=request.data['parent_id'], futureFlag=future,
                          color=color)
                g.save()
                data = {'name': g.name,
                        'id': g.id,
                        'depth': g.get_level(),
                        'tree_id': g.parent.tree_id,
                        'future': g.futureFlag,
                        'color': g.color,
                        'level': g.get_level()
                        }
                return Response(data=data, status=status.HTTP_201_CREATED)

            else:
                return Response(data={'message': 'this node created before'}, status=status.HTTP_409_CONFLICT)

    def delete(self, request):

        obj = get_object_or_404(Genre,
                                id=request.data['node_id'],
                                name=request.data['node_name'],
                                tree_id=request.data['tree_id'])

        obj.delete()
        return Response(data={'ok': 'ok'}, status=status.HTTP_200_OK)

    def put(self, request):

        if request.data['type'] == 'rename':

            obj = get_object_or_404(Genre, id=request.data['node_id'], name=request.data['node_name'],
                                    tree_id=request.data['tree_id'])

            if request.data['future'] == "false":

                future = False
            else:
                future = True

            # obj.name = request.data['new_node_name']
            # obj.futureFlag = future
            # obj.save()
            #
            # return Response(data={'ok': 'ok'}, status=status.HTTP_200_OK)
            print(request.data['color'])
            if not Genre.objects.filter(name=request.data['new_node_name'], parent_id=int(request.data['parent_id']),
                                        futureFlag=future, tree_id=int(request.data['tree_id']),
                                        color=request.data['color']).exists():
                obj.name = request.data['new_node_name']
                obj.futureFlag = future
                obj.color = request.data['color']
                obj.save()

                return Response(data={'ok': 'ok'}, status=status.HTTP_200_OK)

            else:

                return Response(data={'message': 'Dose not exist item'}, status=status.HTTP_400_BAD_REQUEST)

        elif request.data['type'] == 'drag':

            ch_node_name = request.data['ch_node_name']
            ch_node_id = request.data['ch_node_id']
            ch_tree_id = request.data['ch_tree_id']
            p_node_name = request.data['p_node_name']
            p_node_id = request.data['p_node_id']
            p_tree_id = request.data['p_tree_id']

            select_parent = get_object_or_404(Genre, id=p_node_id, name=p_node_name, tree_id=p_tree_id)
            select_child = get_object_or_404(Genre, id=ch_node_id, name=ch_node_name, tree_id=ch_tree_id)

            if select_child.name not in [x[0] for x in Genre.objects.filter(parent=select_parent).values_list('name')]:

                select_child.move_to(select_parent, 'first-child')

                return Response(data={'ok': 'ok'}, status=status.HTTP_200_OK)

            else:
                return Response(data={'message': 'this node exist in child of new parent that selected.'},
                                status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def show_genres2(request):
    if request.method == 'GET':

        Genres = Genre.objects.all()
        root_nodes = cache_tree_children(Genres)
        dicts = []

        for n in root_nodes:
            dicts.append(recursive_node_to_dict(n))

        return Response(dicts[0])


def show_genres(request):
    # print(FullTreeForModelNode(Genre.objects.all()))
    root_nodes = cache_tree_children(Genre.objects.all())

    dicts = []
    for n in root_nodes:
        dicts.append(recursive_node_to_dict(n))

    return render(request, "txt_process/genres.html", dicts[0])


@api_view(['GET', 'POST'])
def get_para_syntactic_tree_data(request, pk):
    if request.method == 'GET':

        dict = {}
        nodes = []
        links = []

        tokens = Token.objects.filter(sentence_id=pk).order_by('pk')

        for token in tokens:

            nodes.append({"id": token.id,
                          "name": token.text})

            if token.source:
                links.append({"source": token.source_id,
                              "target": token.id,
                              "value": token.target_label.name,
                              "color": token.target_label.color})

        if len(tokens) > 0:
            if tokens[0].type == 'I':
                dict["nodes"] = reversed(nodes)
            else:
                dict["nodes"] = nodes

        dict["links"] = links

        return Response({'data': dict})


@api_view(['PUT'])
def delete_token_source_label(request):
    if request.method == 'PUT':
        token = get_object_or_404(Token, user=request.user, pk=request.data['id'])
        token.source = None
        token.target_label = None
        token.save()

        return Response({'data': dict})


@api_view(['PUT'])
def save_token_root(request, pk):
    if request.method == 'PUT':
        token = get_object_or_404(Token, user=request.user, pk=pk)
        token.user_root = request.data['root']
        token.save()

        return Response({'ok': 'ok'})


@api_view(['PUT'])
def edit_sentence(request, pk):
    if request.method == 'PUT':
        sentence = get_object_or_404(Sentence, user=request.user, pk=pk)

        textNormal = Normalizer().normalize(text=request.data['text'])

        sentence.text = textNormal
        sentence.save()

        for token in sentence.token_set.all():
            token.delete()
        text = textNormal.strip()
        for tok in WordTokenizer().tokenize(text):
            root_word = Stemmer().stem(tok)

            tok_obj = Token(user=request.user, document=sentence.document, system_root=root_word,
                            paragraph=sentence.paragraph,
                            sentence=sentence, text=tok)
            tok_obj.save()

        return Response({'ok': 'ok'})


@api_view(['POST'])
def get_sentence(request):

    if request.method == 'POST':
        print(int(request.data['num']))
        sentence = get_object_or_404(Sentence, user=request.user, pk=request.data['pk'])
        sentence = list(Sentence.objects.filter(document_id=sentence.document.pk).order_by('pk'))[int(request.data['num'])-1]

        return Response({'pk': sentence.pk})


def signup(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = RegisterForm()
    #
    # print(form.errors)
    return render(request, 'txt_process/register.html', {'form': form})


def download(request, pk):
    print(settings.STATIC_ROOT)
    filename = make_doc_file(pk=pk)


    file_path = f"{settings.STATIC_ROOT}/txt_process/text_file/{filename}"

    file_wrapper = FileWrapper(open(file_path, 'rb'))
    file_type = mimetypes.guess_type(file_path)
    response = HttpResponse(file_wrapper, content_type=file_type)
    response['X-Sendfile'] = file_path
    response['Content-Length'] = os.stat(file_path).st_size
    response['Content-Disposition'] = f"attachment; filename={str(filename)}"

    return response
