from django.contrib import admin
from .models import Document, Sentence, Paragraph, Token, TreeNode, Genre, FutureValue, DocumentFile

# Register your models here.

admin.site.register(Document)

admin.site.register(Sentence)
admin.site.register(Paragraph)
admin.site.register(Token)
admin.site.register(TreeNode)
admin.site.register(Genre)
admin.site.register(FutureValue)
admin.site.register(DocumentFile)
# admin.site.register(MorphoItems)
