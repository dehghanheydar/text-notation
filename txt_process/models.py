from django.db import models

# color field module
from colorfield.fields import ColorField

# tree module usage
from mptt.models import MPTTModel, TreeForeignKey

from django.conf import settings

User = settings.AUTH_USER_MODEL


class Document(models.Model):
    type_CHOICES = (
        ('F', 'فارسی'),
        ('I', 'آوانگاری شده'),
    )
    user = models.ForeignKey(User,
                             default=1,
                             null=True,
                             on_delete=models.SET_NULL
                             )
    name = models.CharField(max_length=255, blank=True)
    text = models.TextField(default=None)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=10, choices=type_CHOICES, blank=True, null=True)

    def __str__(self):
        return self.name


class Paragraph(models.Model):
    type_CHOICES = (
        ('F', 'فارسی'),
        ('I', 'آوانگاری شده'),
    )
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL)
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    text = models.TextField(default=None
                            , blank=True)
    type = models.CharField(max_length=10, choices=type_CHOICES, blank=True, null=True)

    def __str__(self):
        return self.document.name


class Sentence(models.Model):
    type_CHOICES = (
        ('F', 'فارسی'),
        ('I', 'آوانگاری شده'),
    )
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL)
    document = models.ForeignKey(Document, null=True, on_delete=models.CASCADE)
    paragraph = models.ForeignKey(Paragraph, null=True, on_delete=models.CASCADE)
    text = models.CharField(max_length=1000, blank=True)
    type = models.CharField(max_length=10, choices=type_CHOICES, blank=True, null=True)

    def __str__(self):
        return self.text


class Genre(MPTTModel):
    type_CHOICES = (
        ('F', 'فارسی'),
        ('I', 'آوانگاری شده'),
    )

    name = models.CharField(max_length=50, unique=False,default='---' )
    futureFlag = models.BooleanField(default=False)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    color = ColorField(null=True, blank=True)
    type = models.CharField(max_length=10, choices=type_CHOICES, blank=True, null=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name


class Token(models.Model):
    type_CHOICES = (
        ('F', 'فارسی'),
        ('I', 'آوانگاری شده'),
    )
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL)
    document = models.ForeignKey(Document, null=True, on_delete=models.CASCADE)
    paragraph = models.ForeignKey(Paragraph, null=True, on_delete=models.CASCADE)
    sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE)
    system_root = models.CharField(max_length=40, blank=True, default='---')
    user_root = models.CharField(max_length=40, blank=True)
    source = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)
    target_label = models.ForeignKey(Genre, null=True, blank=True, on_delete=models.SET_NULL)
    tfidf = models.FloatField(default=1.6, blank=True)
    text = models.CharField(max_length=40, blank=True, default='---')
    bigPOS = models.ForeignKey(Genre, null=True, blank=True, on_delete=models.SET_NULL, related_name='big_ops')
    smallPOS = models.ForeignKey(Genre, null=True, blank=True, on_delete=models.SET_NULL, related_name='small_ops')
    type = models.CharField(max_length=10, choices=type_CHOICES, blank=True, null=True)

    class Meta:
        # order on primary key to make sure it's unique
        ordering = ['pk']


class FutureValue(models.Model):
    token = models.ForeignKey(Token, null=True, on_delete=models.CASCADE)
    future = models.ForeignKey(Genre, null=True, blank=True, on_delete=models.SET_NULL, related_name='future')
    futureValue = models.ForeignKey(Genre, null=True, blank=True, on_delete=models.SET_NULL, related_name='future_value')


class DocumentFile(models.Model):
    document = models.ForeignKey(Document, null=True, on_delete=models.CASCADE)
    file = models.FileField()


class TreeNode(models.Model):
    name = models.CharField(max_length=50)
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name




