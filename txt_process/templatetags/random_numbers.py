import random
from django import template
from django.shortcuts import get_object_or_404

from txt_process.models import Genre, Token, Sentence

register = template.Library()


@register.simple_tag
def random_int(a, b=None):
    if b is None:
        a, b = 0, a
    return random.randint(a, b)


@register.simple_tag
def check_future(pk):
    obj = Token.objects.get(pk=pk)
    if obj.bigPOS:
        nods = obj.bigPOS.get_children()
        for child in nods:
            if child.futureFlag:
                return True

    return False


@register.simple_tag
def get_index(pk):
    sentence = Sentence.objects.get(pk=pk)

    return list(sentence.document.sentence_set.all().values_list('id', flat=True)).index(sentence.id) + 1
    # return list(sentence.document.sentence_set.all().reverse()).index(sentence) + 1

