
$(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('keypress',function(e) {
    if(e.which === 13) {

        let model = {
            num : $('#morpho-sentence-num').val(),
            pk : $('#morpho-sentence-pk').text()
        }

        let request = new Request(
         '/get-sentence/',
         {headers: {'X-CSRFToken': csrftokenSetting,
                       'Content-Type': 'application/json'}}
            );

        fetch(request, {
            method: 'POST',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response.json())

            .then((response) => {

                window.location.replace(`/edit-Sentence/${response.pk}/صرفی/`);

            })
            .catch((error) => {
                alert('این جمله وجود ندارد.')
        });


    }
});





function getCookieSetting(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

let csrftokenSetting = getCookieSetting('csrftoken');

function deleteTokenSourceLabel(id){


        let model = {
            "id":id
        }


      let request = new Request(
     '/delete-token-source-label/',
     {headers: {'X-CSRFToken': csrftokenSetting,
                   'Content-Type': 'application/json'}}
        );

        fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response.json())

            .then((response) => {


                $(`#node-label-input-${id}`).val("...")
                $(`#node-select-input-${id}`).val("...")


            })
            .catch((error) => {
                console.log(error)
        });




}

function setRootWord(input_id, root) {

    $(`#root-word-input-${input_id}`).val(root)

}

function saveRootWord(token_pk) {

    let root = $(`#root-word-input-${token_pk}`).val()

       let model = {
            root:root,
       }

       const request = new Request(
            `/save-root/${token_pk}`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}}
       );

       fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


       })
           .then((response) => response.json())
           .then((response)=> {
               $(`#save-root-word-btn-${token_pk}`).addClass('text-success')

           })


}



function selectTargetTokenModal(target, targetId) {

        $("#node-target").text(`${target}`)
        $("#node-target-hidden").text(`${targetId}`)

}

function showTokenModal(source, sourceId) {

        $("#node-source").text(`${source}`)
        $("#node-source-hidden").text(`${sourceId}`)
        $("#node-input-target-hidden").text(`node-select-input-${sourceId}`)
        $("#node-id-hidden").text(`${sourceId}`)
        $("#tokensModalCenter").modal()

}

function checkExist(id) {
    if ($(`#${id}`).length)
        {
         return false;
        }
        else
        {
         return true;
        }
}
// com back little pos
function comeBack(old_select_id, token_pk) {
     let model = {
           token_pk:token_pk,
            type:'smallPOS'
       }

       const request = new Request(
            `/token-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}}
       );

       fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


       })
           .then((response) => response.json())
           .then((response)=> {

               $(`#${old_select_id}`)
                   .val($(`#${old_select_id} > option:first`).val())
                   .prop('disabled', false)
                   .siblings('.come-back').remove()

               $(`#${old_select_id}`).siblings('.remove').css('pointer-events', 'auto').children('i').removeClass('text-muted').addClass('text-danger')



           })





}

// come back big pos
function comeBackPOS(old_select_id, token_pk) {
     let model = {
           token_pk:token_pk,
            type:'bigPOS'
       }

       const request = new Request(
            `/token-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}}
       );

       fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


       })
           .then((response) => response.json())
           .then((response)=> {

               $(`#${old_select_id}`)
                   .val($(`#${old_select_id} > option:first`).val())
                   .prop('disabled', false)
                   .siblings('.come-back').remove()

               $(`#${old_select_id}`).parent().siblings('td#f-pos').remove()



           })





}

// come back future
function comeBackPos(old_select_id) {

    $(`#${old_select_id}`)
        .val($(`#${old_select_id} > option:first`).val())
        .prop('disabled', false)
        .parent().next().remove()
        // .parent().siblings('td a.come-back').remove()

    $(`#${old_select_id}`).siblings('.remove').css('pointer-events', 'auto').children('i').removeClass('text-muted').addClass('text-danger')

}

function remove_option(select_id,old_select_id,token_pk) {

      let model = {
          pk:token_pk,
          type:'morpho'
      }

      const request = new Request(
        `/set-data-edit-syntactic/`,
        {headers: {'X-CSRFToken': csrftokenSetting,
                       'Content-Type': 'application/json'}});

      fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


        })
            .then((response) => response.json())
            .then((response)=> {

                $(`#${old_select_id}`)
                    .val($(`#${old_select_id} > option:first`).val())
                    .prop('disabled', false)
                    .siblings('a.remove').removeAttr('hidden')
                    .siblings('.come-back').remove()


                $(`#${select_id}`)
                    .parent()
                    .parent()
                    .remove('td')



            })
            .catch((error) => {
                console.log(error)
        });







}

function futureRemoveOption(select_id,old_select_id,future_pk) {


        $(`#${old_select_id}`)
        .val($(`#${old_select_id} > option:first`).val())
        .prop('disabled', false)
        .siblings('a').removeAttr('hidden')

        $(`#${select_id}`).parent().remove()







}

function update_future(id_word, random,future_pk) {

       var future = document.getElementById(`future-${random}`);

       let model = {
           future_node_pk:future.value,
           token_pk:id_word,
           future_pk:future_pk,
           type:'F'
       }



       const request = new Request(
            `/future-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}}
       );

       fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


       })
           .then((response) => response.json())
           .then((response)=> {

                // create random id for select and option created
                while (true){

                     var random_id =`select-${id_word}-${Math.floor((Math.random()*10000)+1)}`
                     if (checkExist(`${random_id}`)){
                         break;
                     }
                }

                if (response.childs.length > 0){

                    let future_flag = false
                    let text_future = `<option value="---" style="display: none">---</option>`
                    for  (i = 0; i < response.childs.length; i++){

                        if (response.childs[i].future == true){

                            future_flag = true
                            text_future += `<option value="${response.childs[i].pk}" >${response.childs[i].name}</option>`;

                         }
                    }

                    if(future_flag === true) {

                        text_future = `<td  id="future-table-td-${random_id}" class="">
                                         <a style="width: 10%; font-size: 11px" class="remove" onclick="futureRemoveOption('future-${random_id}', 'future-${random}')" href="#" ><i class="fa fa-remove text-danger" ></i></a> 
                                         <select style="width: 80%; "  id="future-${random_id}" onchange="add_future_value('${id_word}', '${random_id}','${response.future_pk}')" > ${text_future} </select>
                                       </td>`


                        $(`#future-table-tr-${random}`).append( `${text_future}` );

                        // disabel select list
                        future.setAttribute('disabled', true);
                        future.setAttribute('onchange',`update_future('${id_word}', '${random}', '${response.future_pk}')`)

                        // hidden remove button befor selected list
                        $(`#future-${random}`).siblings('.remove').css("pointer-events", "none");
                        // $(`#future-${random}`).siblings('.remove').attr('hidden', 'hidden')

                    }// end if future_flag
                }//end if response.childs.length

                else{



                    $(`#future-table-td-${random}`).parent().append(
                      `<td class="px-1"><a style="width: 7%; font-size: 10px"  class="come-back text-center" id="${random_id}" onclick="comeBackPos('future-${random}')" href="#" >
                        <i class="fa fa-arrow-circle-right text-danger" ></i>
                       </a></td>`);


                    // disabel select list
                    future.setAttribute('disabled', true);


                    // hidden remove button befor selected list
                    // $(`#future-${random}`).siblings('.remove').attr('hidden', 'hidden')
                    $(`#future-${random}`).siblings('.remove')
                        .css('pointer-events', 'none').children('i').removeClass('text-danger').addClass('text-muted');

                }

           })
           .catch((error) => {
                console.log(error)
        });





}

function add_future(id_word, random,future_pk) {

       var future = document.getElementById(`future-${random}`);


       let model = {
           // future_pk:future.value,
           future_node_pk:future.value,
           token_pk:id_word,
           future_pk:future_pk
       }



       const request = new Request(
            `/future-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}}
       );

       fetch(request, {
            method: 'POST',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


       })
           .then((response) => response.json())
           .then((response)=> {

                // create random id for select and option created
                while (true){

                     var random_id =`select-${id_word}-${Math.floor((Math.random()*10000)+1)}`
                     if (checkExist(`${random_id}`)){
                         break;
                     }
                }

                if (response.childs.length > 0){

                    let future_flag = false
                    let text_future = `<option value="---" style="display: none">---</option>`
                    for  (i = 0; i < response.childs.length; i++){

                        if (response.childs[i].future == true){

                            future_flag = true
                            text_future += `<option value="${response.childs[i].pk}" >${response.childs[i].name}</option>`;

                         }
                    }

                    if(future_flag === true) {

                        text_future = `<td  id="future-table-td-${random_id}" class="">
                                         <a style="width: 10%; font-size: 11px" class="remove" onclick="futureRemoveOption('future-${random_id}', 'future-${random}')" href="#" ><i class="fa fa-remove text-danger" ></i></a> 
                                         <select style="width: 80%; "  id="future-${random_id}" onchange="update_future('${id_word}', '${random_id}',null)" > ${text_future} </select>
                                       </td>`


                        $(`#future-table-tr-${random}`).append( `${text_future}` );

                        // disabel select list
                        future.setAttribute('disabled', true);

                        // hidden remove button befor selected list
                        $(`#future-${random}`).siblings('.remove').css("pointer-events", "none");
                        // $(`#future-${random}`).siblings('.remove').attr('hidden', 'hidden')

                    }// end if future_flag
                }//end if response.childs.length

                else{



                    $(`#future-table-td-${random}`).parent().append(
                      `<td class="px-1"><a style="width: 7%; font-size: 10px"  class="come-back text-center" id="${random_id}" onclick="comeBackPos('future-${random}')" href="#" >
                        <i class="fa fa-arrow-circle-right text-danger" ></i>
                       </a></td>`);


                    // disabel select list
                    future.setAttribute('disabled', true);


                    // hidden remove button befor selected list
                    // $(`#future-${random}`).siblings('.remove').attr('hidden', 'hidden')
                    $(`#future-${random}`).siblings('.remove')
                        .css('pointer-events', 'none').children('i').removeClass('text-danger').addClass('text-muted');

                }

           })
           .catch((error) => {
                console.log(error)
        });





}

function add_future_value(id_word, random, future_pk) {

       var future = document.getElementById(`future-${random}`);

       let model = {
           future_value:future.value,
           future_pk:future_pk,
           type:'FV'
       }

       const request = new Request(
            `/future-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}}
       );

       fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


       })
           .then((response) => response.json())
           .then((response)=> {

                // create random id for select and option created
                while (true){

                     var random_id =`select-${id_word}-${Math.floor((Math.random()*10000)+1)}`
                     if (checkExist(`${random_id}`)){
                         break;
                     }
                }

                $(`#future-table-td-${random}`).parent().append(
                  `<td class="px-1"><a style="width: 7%; font-size: 10px"  class="come-back text-center" id="${random_id}" onclick="comeBackPos('future-${random}')" href="#" >
                    <i class="fa fa-arrow-circle-right text-danger" ></i>
                   </a></td>`);


                // disabel select list
                future.setAttribute('disabled', true);


                // hidden remove button befor selected list
                // $(`#future-${random}`).siblings('.remove').attr('hidden', 'hidden')
                $(`#future-${random}`).siblings('.remove')
                    .css('pointer-events', 'none').children('i').removeClass('text-danger').addClass('text-muted');



           })
           .catch((error) => {
                console.log(error)
        });





}

function futureRemoveRow(table_id, token_pk) {

    let model={
        pk:token_pk,
        type:'one'
    }

     const request = new Request(
            `/future-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}});

   fetch(request, {

       method: 'DELETE',
       mode: 'same-origin',  // Do not send CSRF token to another domain.
       body:JSON.stringify(model), // body data type must match "Content-Type" header
   })
       .then((response)=> {
         $(`#future-table-tr-${table_id}`).remove()})
       .catch((error) => {
           console.log(error)
       })

}

function futureRemoveTable(table_id, token_pk) {

    console.log(111)

    let model={
        token_pk:token_pk,
        type:'all'
    }

     const request = new Request(
            `/future-edit/`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}});

   fetch(request, {

       method: 'DELETE',
       mode: 'same-origin',  // Do not send CSRF token to another domain.
       body:JSON.stringify(model), // body data type must match "Content-Type" header
   })
       .then((response)=> {
           $(`#${table_id}`).remove()
       })



}

function futureAddRow(comp_id,token_pk, future_node_pk) {

    // create random id for select and option created

    while (true){
         var future_random_comp_id =`select-${token_pk}-${Math.floor((Math.random()*10000)+1)}`


         if (checkExist(`${future_random_comp_id}`)){

             break;

         }
    }

    // document.getElementById("demo").innerHTML = x.options[i].text;



   const request = new Request(
            `/future-edit/${future_node_pk}`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}});

   fetch(request, {

       method: 'GET',
       mode: 'same-origin',  // Do not send CSRF token to another domain.


   })

       .then((response) => response.json())
       .then((response)=> {
           let futute = false
           let text_future = `<option style="display: none">---</option>`
           for  (i = 0; i < response.childs.length; i++){
               if (response.childs[i].future == true){
                   futute = true
                   text_future += `<option   value="${response.childs[i].pk}" >${response.childs[i].name}</option>`;
               }
   }


   if(futute == true){
         $(`#future-table-${comp_id}`)
             .append(` <tr  id="future-table-tr-${future_random_comp_id}">
                          <td class="px-1" ><a style="font-size: 11px" class="remove" onclick="futureRemoveRow('${future_random_comp_id}')" href="#" ><i class="fa fa-remove text-danger" ></i></a></td> 
                          <td id="future-table-td-${future_random_comp_id}" class=""> 
                                 <select style="width: 100%"  id="future-${future_random_comp_id}" onchange="update_future('${token_pk}', '${future_random_comp_id}', ${null})" > ${text_future} </select>
                           </td>
                           
                       </tr>`)

   }



            });

  }

function add_option(token_pk, select_id, type){

      var x = document.getElementById(select_id);
      // document.getElementById("demo").innerHTML = x.options[i].text;

      const request = new Request(
            `/set-data-edit-syntactic/${x.value}/${token_pk}/${type}`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}});

      fetch(request, {
            method: 'GET',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            // body:JSON.stringify(model), // body data type must match "Content-Type" header


        })
            .then((response) => response.json())
            .then((response)=> {


                // create random id for select and option created
                while (true) {
                    var random_id = `select-${token_pk}-${Math.floor((Math.random() * 10000) + 1)}`

                    if (checkExist(`${random_id}`)) {

                        break;

                    }
                }


                if (response.childs.length > 0) {

                    let futute = false
                    //let pos = false
                    let text_pos = `<option value="---"  style="display: none">---</option>`
                    let text_future = `<option value="---" style="display: none">---</option>`
                    child_list = [];
                    for (i = 0; i < response.childs.length; i++) {

                        if (response.childs[i].future == true) {

                            futute = true

                            text_future += `<option   value="${response.childs[i].pk}" >${response.childs[i].name}</option>`;

                        } else {
                            // pos = true

                            text_pos += `<option   value="${response.childs[i].pk}" >${response.childs[i].name}</option>`;

                        }

                    }

                    if (futute == true) {


                        text_future = `<div class="row px-2 f">
                                 
                                             <table class="table shadow-sm bg-white rounded" id="future-table-${select_id}">
                                             
                                                  <thead>
                                                    <th class="p-1 text-center rounded" style="width: 7%; font-size: 9px"  scope="col"><a class="remove" onclick="futureAddRow('${random_id}','${token_pk}', ${x.value})" href="#" ><i class="fa fa-plus text-info" ></i></a></th>
                                                    <th class="p-1 text-center rounded" style="width: 43%; font-size: 9px" scope="col">فیچر</th>
                                                    <th class="p-1 text-center rounded" style="width: 43%; font-size: 9px"  scope="col">مقدار</th>
                                                    <th class="p-1 text-center rounded" style="width: 7%; font-size: 9px"  scope="col"><a class="remove" onclick="futureRemoveTable('future-table-${select_id}','${token_pk}')" href="#" ><i class="fa fa-trash-o text-danger" ></i></a></th>
                                                    
                                                  </thead>
                                                 
                                                  <tbody id="future-table-${random_id}">
                                                    <tr id="future-table-tr-${random_id}">
                                                      <td class="px-1">
                                                      <a   style="font-size: 11px ;" class="remove " onclick="futureRemoveRow('${random_id}')" href="#" ><i class="fa fa-remove text-danger" ></i></a>
                                                      </td>
                                                      <td id="future-table-td-${random_id}" class=""> 
                                                         <select style="width: 100%" id="future-${random_id}" onchange="update_future('${token_pk}', '${random_id}',null)" > ${text_future} </select>
                                                      </td>
                                                     </tr>
                                                  </tbody>
                                                  
                                             </table>
                                        
                                       </div>`
                    }

                    //if(pos ==true){
                    text_pos = `<div  class="row mb-2 px-2 pos ">
                                    <a  style="width: 7%; font-size: 11px" class="remove text-center pt-1" onclick="remove_option('${random_id}', '${select_id}','${token_pk}')" href="#" ><i class="fa fa-remove text-danger" ></i></a></di> <select  style="width: 43%;" id="${random_id}" onchange="add_option('${token_pk}', '${random_id}','S')"> ${text_pos} </select>
                                 </div>`
                    // }


                    $(`#${token_pk}`).append(
                        `<td id="f-pos" >
                             ${text_pos}
                             ${text_future}
                    </td>`);


                    x.setAttribute('disabled', true);
                    // console.log(x.parent().name)
                    // $(`#future-${random}`).siblings('.remove').css("pointer-events", "none");
                    $(`#${select_id}`).siblings('a').attr('hidden', 'hidden')

                } else {

                    if (type === 'S') {
                        $(`#${token_pk} td#f-pos div.pos `).append(
                            //`<td id="f-pos" ><div  class="row mb-2 px-2 pos ">
                            `<a style="width: 40%" class="come-back text-center " id="${random_id}" onclick="comeBack('${select_id}','${token_pk}')" href="#" >
                                <i class="fa fa-arrow-circle-right text-danger" ></i>
                           </a>`);

                        x.setAttribute('disabled', true);
                        // console.log(x.parent().name)
                        // $(`#future-${random}`).siblings('.remove').attr("disabled","disabled");
                        $(`#${select_id}`).siblings('a.remove')
                            .css('pointer-events', 'none').children('i').removeClass('text-danger').addClass('text-muted');

                    }else if (type === 'B') {

                         $(`#${token_pk}`).append(
                            `<td id="f-pos" ><div  class="row mb-2 px-2 pos ">
                            <a style="width: 40%" class="come-back text-center " id="${random_id}" onclick="comeBackPOS('${select_id}','${token_pk}')" href="#" >
                                <i class="fa fa-arrow-circle-right text-danger" ></i>
                             </a> </div> </td>`);

                        x.setAttribute('disabled', true);
                        // console.log(x.parent().name)
                        // $(`#future-${random}`).siblings('.remove').attr("disabled","disabled");
                        $(`#${select_id}`).siblings('a.remove')
                            .css('pointer-events', 'none').children('i').removeClass('text-danger').addClass('text-muted');

                }

            }



            })
            .catch((error) => {
                console.log(error)
        });



}

function saveChangeSentenceText(pk){
         let text = $('#modalEditSentenceTextArea').val()


          let request = new Request(
            `/new-sentence/${pk}`,
            {headers: {'X-CSRFToken': csrftokenSetting,
                           'Content-Type': 'application/json'}});
          let model = {
              text : text
          }


          fetch(request, {
                method: 'PUT',
                mode: 'same-origin',  // Do not send CSRF token to another domain.
                body:JSON.stringify(model), // body data type must match "Content-Type" header


            })
                .then((response) => response.json())
                .then((response)=> {
                    $('#modalEditSentence').modal('toggle');

                    location.reload()
                })

}

mouseXPosition = 0;
function addNottaion(color, para_id) {
                    var highlighted = false;
                    var selection = window.getSelection();
                    var selectedText = selection.toString();
                    var startPoint = window.getSelection().getRangeAt(0).startOffset;
                    var endPoint = window.getSelection().getRangeAt(0).endOffset;
                    var anchorTag = selection.anchorNode.parentNode;
                    var focusTag = selection.focusNode.parentNode;

                    // if ((e2.pageX - mouseXPosition) < 0) {
                    //     focusTag = selection.anchorNode.parentNode;
                    //     anchorTag = selection.focusNode.parentNode;
                    // }
                    if (selectedText.length === (endPoint - startPoint)) {
                        console.log(111)
                        highlighted = true;

                        if (anchorTag.className !== "highlight") {

                            highlightSelection(color,para_id);
                        } else {
                            alert('انتخاب متن غیرمجاز!')
                            // var afterText = selectedText + "<span style='background-color:"+color+"' class = 'highlight'>" + anchorTag.innerHTML.substr(endPoint) + "</span>";
                            // anchorTag.innerHTML = anchorTag.innerHTML.substr(0, startPoint);
                            // anchorTag.insertAdjacentHTML('afterend', afterText);
                        }

                    }else{
                        if(anchorTag.className !== "highlight" && focusTag.className !== "highlight"){
                            highlightSelection(color,para_id);
                            highlighted = true;
                        }

                    }

                    if (anchorTag.className === "highlight" && focusTag.className === 'highlight' && !highlighted) {
                        highlighted = true;
                        console.log(222)

                        var afterHtml = anchorTag.innerHTML.substr(startPoint);
                        var outerHtml = selectedText.substr(afterHtml.length, selectedText.length - endPoint - afterHtml.length);
                        var anchorInnerhtml = anchorTag.innerHTML.substr(0, startPoint);
                        var focusInnerHtml = focusTag.innerHTML.substr(endPoint);
                        var focusBeforeHtml = focusTag.innerHTML.substr(0, endPoint);
                        selection.deleteFromDocument();
                        anchorTag.innerHTML = anchorInnerhtml;
                        focusTag.innerHTml = focusInnerHtml;
                        var anchorafterHtml = afterHtml + outerHtml + focusBeforeHtml;
                        anchorTag.insertAdjacentHTML('afterend', anchorafterHtml);


                    }

                    if (anchorTag.className === "highlight" && !highlighted) {
                        console.log(333)
                        highlighted = true;
						var Innerhtml = anchorTag.innerHTML.substr(0, startPoint);
                        var afterHtml = anchorTag.innerHTML.substr(startPoint);
                        var outerHtml = selectedText.substr(afterHtml.length, selectedText.length);
                        selection.deleteFromDocument();
                        anchorTag.innerHTML = Innerhtml;
                        anchorTag.insertAdjacentHTML('afterend', afterHtml + outerHtml);
                     }


                    if (focusTag.className === 'highlight' && !highlighted) {

                        alert('انتخاب متن غیرمجاز!')


                        // highlighted = true;
						// var beforeHtml = focusTag.innerHTML.substr(0, endPoint);
                        // var outerHtml = selectedText.substr(0, selectedText.length - beforeHtml.length);
                        // selection.deleteFromDocument();
                        // focusTag.innerHTml = focusTag.innerHTML.substr(endPoint);
                        // outerHtml += beforeHtml;
                        // focusTag.insertAdjacentHTML('beforebegin', outerHtml );
                        //

                    }

                    if (!highlighted) {

                        highlightSelection(color,para_id);
                    }

                    $('.highlight').each(function(){
                        if($(this).html() == ''){
                            $(this).remove();
                        }
                    });
                    selection.removeAllRanges();
                }

function removeHighlightSelection(id) {

    let text = $(`#${id}`).text()

    $(`#${id}`).replaceWith(text)
}

function highlightSelection(color,para_id) {
    var selection;

    //Get the selected stuff
    if (window.getSelection)
        selection = window.getSelection();
    else if (typeof document.selection != "undefined")
        selection = document.selection;

    //Get a the selected content, in a range object
    var range = selection.getRangeAt(0);

    //If the range spans some text, and inside a tag, set its css class.
    if (range && !selection.isCollapsed) {
        if (selection.anchorNode.parentNode === selection.focusNode.parentNode) {
            var span = document.createElement('span');
            span.className = 'highlight';
            span.style = `background-color:${color}`
            while (true){
                var rand_id = `span-${para_id}-${Math.floor((Math.random()*100)+1)}`
                if (checkExist(rand_id)){
                    break
                }
            }
            span.id = rand_id
            span.textContent = selection.toString();
            var a = document.createElement('a')
            a.className = 'mx-1'
            a.setAttribute('href', '#')
            a.onclick = function () {
                 let text = $(`#${rand_id}`).text()

                 $(`#${rand_id}`).replaceWith(text)
            }
            var i = document.createElement('i')
            i.className = 'fa fa-remove text-light'
            a.appendChild(i)
            span.insertBefore(a,span.firstChild)
            selection.deleteFromDocument();
            range.insertNode(span);
                       // range.surroundContents(span);
        }
    }
}


