$(document).ready(function(){

        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
          let fileName = $(this).val().split("\\").pop();
          $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });


});

function getCookieSetting(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

let csrftokenSetting = getCookieSetting('csrftoken');


function createFile(pk) {

        let request = new Request(
            `/create-file/${pk}`,
            {
                headers: {
                    'X-CSRFToken': csrftokenSetting,
                    'Content-Type': 'application/json'
                }
            }
        );

        fetch(request, {
            method: 'GET',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            // body: JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response.json())

            .then((response) => {
                console.log(response.path)
              window.open(`${response.path}`, null);
            })
            .catch((error) => {
                console.log(error)
            });
}

function deleteDocument() {

    let pk = $('#modal-delete-document-label-doc-pk').text()

        let request = new Request(
            `/ducument/${pk}/`,
            {
                headers: {
                    'X-CSRFToken': csrftokenSetting,
                    'Content-Type': 'application/json'
                }
            }
        );

        fetch(request, {
            method: 'DELETE',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            // body: JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response)

            .then((response) => {

                if($(`#doc-${pk}`).hasClass('my-active')){

                    $('#text-place').empty()
                    $('#s_b_show label').text('')
                    window.location= '/';


                }

                $(`#doc-${pk}`).remove();

                $('#modal-delete-document').modal('hide');


            })
            .catch((error) => {
                console.log(error)
            });
}

function showModalDeleteDocument(pk, name) {

    $('#modal-delete-document-label-doc-name').text(name )
    $('#modal-delete-document-label-doc-pk').text(pk)
    $('#modal-delete-document').modal('show')
}