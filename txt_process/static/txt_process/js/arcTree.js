function getCookieSetting(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

let csrftokenSetting = getCookieSetting('csrftoken');

function drawArcTree(url, pk, div_id) {


    function deletePath(data) {

        let model = {
            data:data,
            type: 'source&target'
        }

        let request = new Request(
            '/token-edit/',
            {
                headers: {
                    'X-CSRFToken': csrftokenSetting,
                    'Content-Type': 'application/json'
                }
            }
        );

        fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body: JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response.json())

            .then((response) => {

                console.log(response)


                drawArcTree('sentence-syntactic-tree-data', `${response.sentence_pk}`, 'card-body')

            })
            .catch((error) => {
                console.log(error)
            });


    }


    d3.json(`/${url}/${pk}`, function (error, graph) {
        if (error) throw error;

        $(`#${div_id} svg`).empty()

        graph = graph.data

        var i, j, node;

        var nodeRadius = d3.scaleSqrt().range([3, 7]);

        var linkWidth = d3.scaleLinear().range([1.5, 2 * nodeRadius.range()[0]]);

        wordWidth = 30;

        wordHeight = 10;

        var margin = {
            top: nodeRadius.range()[1] + 1,
            right: nodeRadius.range()[1] + 1,
            bottom: nodeRadius.range()[1] + 1,
            left: nodeRadius.range()[1] + 1
        };

        var width = 50
        for (i = 0, j = 0; i < graph.nodes.length; ++i) {
            width += (graph.nodes[i].name.length * 6 + 30)
        }

        // var width =  wordWidth * graph.nodes.length - wordWidth  ;
        // var width = d3.select(`#${div_id}`).node().getBoundingClientRect().width ;
        // var height = width/2 ;

        var height = (d3.select(`#${div_id}`).node().getBoundingClientRect().height) * (1.5);

        // var x = d3.scaleLinear().range([0, width]);

        var svg = d3.select(`#${div_id} svg`)
            .attr('style', 'padding:2px')
            .attr('width', width)
            .attr('height', height / 2 + margin.top + margin.bottom + 20)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        var idToNode = {};

        graph.nodes.forEach(function (n) {
            idToNode[n.id] = n;
        });

        graph.links.forEach(function (e) {
            e.source = idToNode[e.source];
            e.target = idToNode[e.target];
        });

        graph.nodes.reverse()
        var my_x = 5
        // Compute x,y coordinates (have a little extra separation when we switch volumes)
        for (i = 0, j = 0; i < graph.nodes.length; ++i) {
            node = graph.nodes[i];
            my_x += (graph.nodes[i].name.length * (graph.nodes[i].name.length <= 3? 12 : 6) + 30)
            node.x = my_x-60;
            node.y = height / 2;
        }


        linkWidth.domain(d3.extent(graph.links, function (d) {
            return 5;
        }));

        var link = svg.append('g')
            .attr('class', 'links')
            .selectAll('path')
            .data(graph.links)
            .enter().append('path')
            .attr('d', function (d) {
                return ['M', d.source.x,
                    height / 2,
                    'A', (d.source.x - d.target.x) / 3, Math.abs(d.source.x - d.target.x) / 2 > height  ? Math.abs(d.source.x - d.target.x) / 30 : Math.abs(d.source.x - d.target.x) / 6,
                    0, 0, d.source.x < d.target.x ? 1 : 0,
                    d.target.x,
                    height / 2].join(' ');
            })
            .style('stroke',function (d){ return d.color} )
            // (((d.source.x - d.target.x) / 6) > height/2) ? ((d.source.x - d.target.x) / 30) :((d.source.x - d.target.x) / 6)
            .attr('stroke-width', function (d) {
                return linkWidth(2);
            })
            .on('mouseover', function (d) {
                // link.style('stroke', null);
                d3.select(this).style('stroke', '#2356d6');
                node.style('fill', function (node_d) {
                    return node_d === d.source || node_d === d.target ? 'black' : null;
                });
            })

            .on('mouseout', function (d) {
                d3.select(this).style('stroke', d.color)
                node.style('fill', null);
                d3.selectAll('path.delete-flag').attr('stroke-width', '4').style('stroke', '#d62338')
            }).on('click', function (d) {

                if (this.classList.contains("delete-flag")) {
                    d3.select(this).attr("class", "").attr('stroke-width', '2').style('stroke', d.color)
                } else {
                    d3.select(this).attr("class", "delete-flag").attr('stroke-width', '4').style('stroke', '#d62338')
                }


            }).on("keydown", function () {
                console.log(d3.event.keyCode)
            });


        var node = svg.append('g')
            .attr('class', 'labels text-right')
            .selectAll('text')
            .data(graph.nodes)
            .enter().append('text')
            .attr('x', function (d) {
                return d.x;
            })
            .text(function (d) {
                return d.name
            })
            .attr('y', function (d) {
                return d.y + 13;
            })
            .style('font-size', '11px')

            .on('mouseover', function (d) {
                node.style('fill', null);
                d3.select(this).style('fill', '#2356d6').style('font-size', '13px');
                var nodesToHighlight = graph.links.map(function (e) {
                    return e.source === d ? e.target : 0
                })
                    .filter(function (d) {
                        return d;
                    });
                node.filter(function (d) {
                    return nodesToHighlight.indexOf(d) >= 0;
                })
                    .style('fill', '#b30505');
                link.style('stroke', function (link_d) {
                    return link_d.source === d | link_d.target === d ? '#2356d6' : link_d.color;
                })

                d3.selectAll(`.w-a-s-${d.id}`)
                    .attr('fill', '#2356d6')

                d3.selectAll(`.wv${d.id}`)
                    .attr('fill', '#2356d6')

            })
            .on('mouseout', function (d) {
                d3.select(this).style('font-size', '11px');
                node.style('fill', null);
                link.style('stroke', function (link_d) {
                    return link_d.color;
                })

                d3.selectAll(`.w-a-s-${d.id}`)
                    .attr('fill',  function (d) {
                        return d.color
                    })

                d3.selectAll(`.wv${d.id}`)
                    .attr('fill', function (d) {
                        return d.color
                    })

                d3.selectAll('path.delete-flag').attr('stroke-width', '4').style('stroke', '#d62338')
            })
            .on('click', function (d) {
                if (this.classList.contains('can-select')) {
                    d3.selectAll('text')
                        .style('font-size', '11px')
                        .style('text-shadow', '3px 0px 4px rgba(7, 126, 133, 1)')
                        .attr('class', "")

                    $('#modalSyntacticNodeTargetIdHidden').text(`${d.id}`)
                    $('#modalSyntacticNodeTarget').text(`${d.name}`)
                    $('#modalSyntacticLabelPath').modal()


                } else {
                    d3.selectAll('text')
                        .style('font-size', '11px')
                        .style('text-shadow', '3px 0px 4px rgba(7, 126, 133, 1)')
                        .attr('class', 'can-select')

                    $('#modalSyntacticNodeSourceIdHidden').text(`${d.id}`)
                    $('#modalSyntacticNodeSource').text(`${d.name}`)

                    d3.select(this).attr('class', "")
                        .style('font-size', '9px')
                        .style('text-shadow', '-75px 3px 0px rgba(133, 126, 133, 0)')
                }

            });


        node.append('title').text(function (d) {
            return d.name;
        });

        // var triangle = d3.svg.symbol().type('triangle-up').size(5);
        svg.append('g')
            .attr('class', 'arrow')
            .selectAll('path')
            .data(graph.links)
            .enter().append("path")
            .attr('class', function (d) {
                return `arrow w-a-s-${d.source.id} w-a-s-${d.target.id}`;
            })
            .attr("d", "M0,-5L10,0L0,5")
            // .attr("d", "M0,0 L0,10 L10,5 L0,0")
            .attr('transform', function (d) {
                return `translate( ${(d.source.x + d.target.x) / 2} ,  ${d.target.y - (Math.abs(d.source.x - d.target.x) / 2 > height  ? Math.abs(d.source.x - d.target.x) / 20 : Math.abs(d.target.x - d.source.x) / 4)} )  rotate(${d.source.x > d.target.x ? '180' : '0'})`
                // return `translate( ${(d.source.x + d.target.x) / 2} ,  ${(d.target.y - ((d.source.x > d.target.x ? (d.source.x - d.target.x) : (d.target.x - d.source.x)) /20))} )  rotate(${d.source.x > d.target.x ? '180' : '0'})`
            })
            .attr('fill', function (d) {
                return d.color
            }).attr('stroke-width', '1');

        // var triangle = d3.svg.symbol().type('triangle-up').size(5);
        svg.append('g')
            .attr('class', 'labels')
            .selectAll('text')
            .data(graph.links)
            .enter().append("text")
            .attr('class', function (d) {
                return "wv" + d.source.id + " wv" + d.target.id;
            })
            .text(function (d) {
                return d.value
            })
            .attr('transform', function (d) {
                return `translate( ${(d.source.x + d.target.x) / 2} ,  ${d.target.y - (Math.abs(d.source.x - d.target.x) / 2 > height  ? Math.abs(d.source.x - d.target.x) / 20 : Math.abs(d.target.x - d.source.x) / 4) - 6} ) `
            })
            .attr('fill', function (d) {
                return d.color
            })
            .attr('stroke-width', '2');


        d3.select("body")
            .on("keydown", function () {

                if (d3.event.keyCode == 27) {
                    d3.selectAll("path.delete-flag")
                        .attr('class', "")
                        .attr('stroke-width', '2')
                        .style('stroke', function (d){
                            return d.color
                        })
                    d3.selectAll("text.can-select")
                        .style('font-size', '11px')
                        .style('text-shadow', '')
                        .attr('class', "")

                } else if (d3.event.keyCode == 46) {
                    deletePath(d3.selectAll("path.delete-flag").data())
                }
                console.log(d3.event.keyCode)
            })

    });

}