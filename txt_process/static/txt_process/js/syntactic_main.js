
$(document).on('keypress',function(e) {
    if(e.which === 13) {

        let model = {
            num : $('#syntactic-sentence-num').val(),
            pk : $('#syntactic-sentence-pk').text()
        }

        let request = new Request(
         '/get-sentence/',
         {headers: {'X-CSRFToken': csrftokenSetting2,
                       'Content-Type': 'application/json'}}
            );

        fetch(request, {
            method: 'POST',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response.json())

            .then((response) => {

                window.location.replace(`/syntactic-edit/${response.pk}`);

            })
            .catch((error) => {
                alert('این جمله وجود ندارد.')
        });


    }
});

function getCookieSetting(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

let csrftokenSetting2 = getCookieSetting('csrftoken');


function saveSourceTargetTokenModal2(sentence_pk) {

       let target =  $("#modalSyntacticNodeTarget").text()
       let target_id = $("#modalSyntacticNodeTargetIdHidden").text()

       let source =  $("#modalSyntacticNodeSource").text()
       let source_id = $("#modalSyntacticNodeSourceIdHidden").text()

       // let label = $('#modalSyntacticLabelSelectOption option:selected').text()
       let label_pk = $('#modalSyntacticLabelSelectOption option:selected').val()

        let model = {
            target:target,
            target_id:target_id,
            source :source,
            source_id:source_id,
            label_pk: label_pk,
            type :'syntactic'



        }

        let request = new Request(
         '/set-data-edit-syntactic/',
         {headers: {'X-CSRFToken': csrftokenSetting2,
                       'Content-Type': 'application/json'}}
            );

        fetch(request, {
            method: 'PUT',
            mode: 'same-origin',  // Do not send CSRF token to another domain.
            body:JSON.stringify(model), // body data type must match "Content-Type" header


        }).then((response) => response.json())

            .then((response) => {

                $("#modalSyntacticLabelPath").modal('toggle');
                drawArcTree('sentence-syntactic-tree-data',`${sentence_pk}`,'card-body')

            })
            .catch((error) => {
                console.log(error)
        });

}


