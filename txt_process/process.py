import os
import sys

from django.core.files import File
from idna import unicode

from TextNotation import settings

from .models import Document, DocumentFile
from tabulate import tabulate

def recursive_node_to_dict(node):
    # if node.get_ancestore(ascending=True, include_self=False):
    #     print(node.get_ancestors(ascending=False, include_self=False)[0])
    result = {
        "name": node.name,
        'id': node.pk,
        'tree_id': node.tree_id,
        'future': node.futureFlag,
        'color': node.color,
        'level': node.level
        # 'root': node.get_ancestors(ascending=False, include_self=False)

        # 'name': node.name,
    }
    children = [recursive_node_to_dict(c) for c in node.get_children()]
    if children:
        result['children'] = children
    return result


def create_file(pk):
    obj = Document.objects.get(pk=pk)
    file1 = open(os.path.join(settings.MEDIA_ROOT, f"txt_process/file_one/{obj.name.replace(' ', '')}_{obj.pk}.txt"),
                 "w+")
    file1.close()


def print_table(table, ):
    headers = ['world', 'POS', 'pos', 'future']

    texh = ''.join(column.rjust(20) for column in headers)
    for row in table:
        tex = ''.join(str(column).rjust(20) for column in row)

    return texh + '\n' + tex


def make_doc_file2(pk):
    # table item length
    til = 10
    obj = Document.objects.get(pk=pk)
    name = f"{obj.pk}.txt"

    path = os.path.join(settings.STATIC_ROOT, f"txt_process/text_file/{name}")
    # path = os.path.join(settings.MEDIA_ROOT, f"txt_process/file_one/{name}")
    # file1 = open(path, "w+")
    # doc_file, create = DocumentFile.objects.get_or_create(document_id=obj.pk, defaults={'file': File(file1)})
    # file1.close()
    # #
    # if create or not doc_file.file:
    #     file1 = open(path, "ab+")
    #     # doc_file.file = File(file1)
    #     doc_file.file = File(file1)
    #     doc_file.save()
    #     file1.close()

    headers = ['index', 'world', 'root', 'POS', 'pos', 'future']

    texh = ''.join(column.ljust(10) for column in headers)

    file1 = open(path, "w+")
    for para in obj.paragraph_set.all():
        for sentence in para.sentence_set.all().reverse():
            file1.buffer.write(f"{texh} \n".encode('utf-8'))
            for index, token in enumerate(sentence.token_set.all()):
                future = None

                if token.futurevalue_set.all().exists():
                    future = ''.join(
                        f"{str(column.future.name)} = {str(column.futureValue is not None and column.futureValue.name or '---')} |"
                        for column in token.futurevalue_set.all())

                text = ''.join(column.ljust(length) for column, length in [(str(index), til), (token.text, til), (
                    token.user_root is None and token.user_root or (
                            token.system_root is None and token.system_root or '---'), til), (
                                                                               token.bigPOS is None and '---' or token.bigPOS.name,
                                                                               til), (
                                                                               token.smallPOS is None and '---' or token.smallPOS.name,
                                                                               til),
                                                                           future is not None and (future, 60) or (
                                                                               '---', 20)])

                file1.buffer.write(f"{text} \n".encode('utf-8'))

            file1.buffer.write('\n \n'.encode('utf-8'))

    file1.close()  # to change file access modes
    return name


def make_doc_file(pk):

    obj = Document.objects.get(pk=pk)
    name = f"{obj.pk}.txt"

    path = os.path.join(settings.STATIC_ROOT, f"txt_process/text_file/{name}")

    headers = ['index', 'world', 'root', 'POS', 'pos', 'future']
    tabels = []

    for para in obj.paragraph_set.all():
        for sentence in para.sentence_set.all().reverse():
            tokens = []
            for token in sentence.token_set.all():
                future = '---'

                if token.futurevalue_set.all().exists():
                    future = ''.join(
                        f"{str(column.future.name)} = {str(column.futureValue is not None and column.futureValue.name or '---')} |"
                        for column in token.futurevalue_set.all())

                tokens.append([
                                token.text,
                                token.user_root,
                                # token.user_root is None and token.user_root or (token.system_root is None and token.system_root or '---'),
                                token.bigPOS is None and '---' or token.bigPOS.name,
                                token.smallPOS is None and '---' or token.smallPOS.name,
                                future])

            tabels.append(tokens)

    file1 = open(path, "w+")

    for tabel in tabels:

        tabel_str = tabulate(tabel, headers=headers, showindex="always")
        file1.buffer.write(f"{tabel_str}\n\n\n".encode('utf-8'))

    file1.close()  # to change file access modes
    return name
