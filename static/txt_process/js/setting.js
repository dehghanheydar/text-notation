
function getCookieSetting(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const csrftokenSetting = getCookieSetting('csrftoken');

function shortener_text(text, max, nice){ if(text.length >= max){ return text.slice(0, nice)+'...';}else {return text} }

function nnn(){
    var name = $('#inputRootNodeName').val()


              var model = {

                  parent_id: 'root',
                  parent_name: null,
                  node_name : name,
                  future:false,
              }

             $.ajax({
                    url:'../../createNode/',
                    type: "POST",
                    dataType: 'json',
                    headers:{'X-CSRFToken': csrftokenSetting},
                    data : model,

                success: function (data) {
                    $('#inputRootNodeName').val('');
                    $('.root_list').append("<li id='"+data.id+"' class=\"list-group-item list-group-item-action text-center px-0 py-3\">" +
                        "<div class=\"row m-0 p-0\">" +
                          " <div class=\"col-3 px-1\">  <i onclick=\"get_tree('"+data.name+"','"+data.id+"')\" class=\"fa fa-arrow-circle-left text-info\" ></i></div>" +
                          "<div class=\"col-3 px-1\">   <i onclick=\"delete_root_node('"+data.name+"','"+data.id+"', '"+data.tree_id+"')\" class=\"fa fa-trash text-danger\" ></i> </div>  " +
                          "<div class=\"col-3 px-1\">   <a onclick=\"get_tree('"+data.name+"')\">"+ shortener_text( data.name, 6, 4)+"  </a></div>" +
                          " <div class=\"col-3 px-1\">  <i class=\"fa fa-circle-o text-info\" aria-hidden=\"true\"></i></div>" +
                        "  </div> " );

                    alert('نود '+data.name+'  با موفقیت ایجاد شد.')
                },
                error: function (data) {

                    alert('عملیات ایجاد نود جدید با مشکل مواجه شد.')
                }

            })
}

function delete_root_node(name, id, tree_id) {

               var model = {
                  node_id: id,
                  node_name: name,
                  tree_id: tree_id,

                  }
               $.ajax({
                        url:'../../createNode/',
                        type: 'DELETE',
                        dataType: 'json',
                        headers:{'X-CSRFToken': csrftoken},
                        data : model,

                        success: function (data) {

                            $('#'+id).remove()

                            $('#tree-container').remove()

                            $('#tree_place').append('<div class="" id="tree-container"> </div>')


                            alert(  'نود '+ name +' با موفقیت حذف شد')



                        },
                        error: function (data) {

                            alert('عملیات حذف با مشکل روبرو شد.')


                        }

                    });


    }
